#pragma once


class Tree
{
private:
	int count;
	int **arr;
	int num;
public:
	Tree();
	void add(int val, int i);
	int add2(int val);
	void showarr();
	void directTravel(int i);
	void symmetryTravel(int i);
	void intersection(Tree T);
	void updatearr();
	void deletearr();
};